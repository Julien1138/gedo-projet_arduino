#ifndef _CONTROLEUR_H
#define _CONTROLEUR_H

#include "Config.h"
#include "Canon.h"
#include "GPS.h"
#include "Memoire.h"

class Config;

class Controleur {
  private:
    Config*   m_pConfig;
    Canon*    m_pCanon;
    GPS*      m_pGPS;
    Memoire*  m_pMemoire;

  public:
    Controleur();
    void init();
    void loop();
    
  public:
    bool clrZonesInMemory();
    bool addZoneInMemory(long lLatitude_1, long lLongitude_1,
                         long lLatitude_2, long lLongitude_2,
                         long lLatitude_3, long lLongitude_3,
                         long lLatitude_4, long lLongitude_4);
};
#endif


