#include "Config.h"
#include "Zone.h"

// Affichage débug !!!!!!!! A DESACTIVER ABSOLUEMENT EN STANDALONE !!!!!!!!! (pas de connexion Série = blocage sur le 'while (!SerialUSB);')
#define DEBUG_VERBOSE

#define CONFIG_TIMEOUT_MS  30000  // 30 secondes
#define ERROR_MESSAGE      "$ERR*45"

#define ADDR_NBR_OF_ZONES  0

//  $CONFIG     // Message d'effacement de début de configuration
//  *0A         // Checksum

//  $CLRZN      // Message d'effacement des zones programmées
//  *49         // Checksum

//  $CFGZN      // Message de configuration d'une zone
//  ,saaaaaaaa  // Latitude du point 1
//  ,sooooooooo // Longitude du point 1
//  ,saaaaaaaa  // Latitude du point 2
//  ,sooooooooo // Longitude du point 2
//  ,saaaaaaaa  // Latitude du point 3
//  ,sooooooooo // Longitude du point 3
//  ,saaaaaaaa  // Latitude du point 4
//  ,sooooooooo // Longitude du point 4
//  *CC         // Checksum

//  $RDZN       // Message de lecture d'une zone
//  0           // Numéro de la zone
//  *CC         // Checksum

Config::Config(Controleur *pControleur)
  :m_pControleur(pControleur)
  ,m_IsConfiguring(true)
{
}

void Config::init()
{
#ifdef DEBUG_VERBOSE
  SerialUSB.println("Init Config :");
  SerialUSB.print(" SerialUSB link...");
#endif

  m_IsConfiguring = false;
  SerialUSB.begin(9600);
  while (!SerialUSB)
  {
    if (millis() > CONFIG_TIMEOUT_MS)
    {
#ifdef DEBUG_VERBOSE
      SerialUSB.println(" Abort");
#endif
      break;
    }
  };
  
  if (SerialUSB)
  {
#ifdef DEBUG_VERBOSE
    SerialUSB.println(" OK");
    SerialUSB.print(" Start config...");
#endif
    while (!m_IsConfiguring)
    {
      read();
      if (millis() > CONFIG_TIMEOUT_MS)
      {
#ifdef DEBUG_VERBOSE
        SerialUSB.println(" Abort");
#endif
        break;
      }
    }
  }

  m_sBuffer = "";

#ifdef DEBUG_VERBOSE
  if (m_IsConfiguring)
  {
    SerialUSB.println(" OK");
  }
#endif
}


void Config::read()
{
  if (SerialUSB.available())
  {
    char c = SerialUSB.read();

    if (c == '\n')
    {
      if (messageIsValid())
      {
        parseCommand();
      }
      m_sBuffer = "";
    }
    else
    {
      m_sBuffer += c;
    }
  }
}

bool Config::isConfiguring()
{
  return m_IsConfiguring;
}


bool Config::messageIsValid()
{
  char checksum = 0;
  for (int i = 1 ; i < m_sBuffer.indexOf('*') ; i++)
  {
    checksum ^= m_sBuffer.charAt(i);
  }
  String str_checksum = String(checksum, HEX);
  if (str_checksum.length() == 1)
  {
    str_checksum = '0' + str_checksum;
  }

  int indexOfChecksum = m_sBuffer.indexOf('*') + 1;
#ifdef DEBUG_VERBOSE
  SerialUSB.print("Calculated Checksum = ");
  SerialUSB.println(str_checksum);
  SerialUSB.print("Received Checksum = ");
  SerialUSB.println(m_sBuffer.substring(indexOfChecksum, indexOfChecksum + 2));
#endif

  if (m_sBuffer.substring(indexOfChecksum, indexOfChecksum + 2).equalsIgnoreCase(str_checksum))
    return true;

#ifdef DEBUG_VERBOSE
  SerialUSB.println("Message is not valid");
#endif
  return false;
}

void Config::parseCommand()
{
#ifdef DEBUG_VERBOSE
  SerialUSB.print("Message type = ");
  SerialUSB.println(m_sBuffer.substring(0, m_sBuffer.indexOf(',')));
#endif
  if (m_sBuffer.substring(0, m_sBuffer.indexOf('*')) == "$CONFIG")  // Passage en mode configuration
  {
    m_IsConfiguring = true;
    SerialUSB.println(F("$ACK,CONFIG*6F"));
  }
  else if (m_sBuffer.substring(0, m_sBuffer.indexOf('*')) == "$CLRZN")  // Effacement des zones programmées
  {
    if (m_pControleur->clrZonesInMemory())
    {
      SerialUSB.println(F("$ACK,CLRZN*2C"));
    }
    else
    {
      SerialUSB.println(F(ERROR_MESSAGE));
    }
  }
  else if (m_sBuffer.substring(0, m_sBuffer.indexOf(',')) == "$CFGZN")  // Configuration d'une zone d'activation
  {
    long lLatitude_1;
    long lLongitude_1;
    long lLatitude_2;
    long lLongitude_2;
    long lLatitude_3;
    long lLongitude_3;
    long lLatitude_4;
    long lLongitude_4;
    
    m_sBuffer.remove(0, m_sBuffer.indexOf(',') + 1);  // Remove $CFGZN
#ifdef DEBUG_VERBOSE
    SerialUSB.print("lLatitude_1 = ");
    SerialUSB.println(m_sBuffer.substring(0, m_sBuffer.indexOf(',')));
    SerialUSB.println(m_sBuffer.substring(0, m_sBuffer.indexOf(',')).toInt());
#endif
    lLatitude_1 = m_sBuffer.substring(0, m_sBuffer.indexOf(',')).toInt();
    m_sBuffer.remove(0, m_sBuffer.indexOf(',') + 1);  // Remove lLatitude_1
    lLongitude_1 = m_sBuffer.substring(0, m_sBuffer.indexOf(',')).toInt();
    m_sBuffer.remove(0, m_sBuffer.indexOf(',') + 1);  // Remove lLongitude_1
    lLatitude_2 = m_sBuffer.substring(0, m_sBuffer.indexOf(',')).toInt();
    m_sBuffer.remove(0, m_sBuffer.indexOf(',') + 1);  // Remove lLatitude_2
    lLongitude_2 = m_sBuffer.substring(0, m_sBuffer.indexOf(',')).toInt();
    m_sBuffer.remove(0, m_sBuffer.indexOf(',') + 1);  // Remove lLongitude_2
    lLatitude_3 = m_sBuffer.substring(0, m_sBuffer.indexOf(',')).toInt();
    m_sBuffer.remove(0, m_sBuffer.indexOf(',') + 1);  // Remove lLatitude_3
    lLongitude_3 = m_sBuffer.substring(0, m_sBuffer.indexOf(',')).toInt();
    m_sBuffer.remove(0, m_sBuffer.indexOf(',') + 1);  // Remove lLongitude_3
    lLatitude_4 = m_sBuffer.substring(0, m_sBuffer.indexOf(',')).toInt();
    m_sBuffer.remove(0, m_sBuffer.indexOf(',') + 1);  // Remove lLatitude_4
    lLongitude_4 = m_sBuffer.substring(0, m_sBuffer.indexOf(',')).toInt();
    
    if (m_pControleur->addZoneInMemory(lLatitude_1, lLongitude_1,
                                       lLatitude_2, lLongitude_2,
                                       lLatitude_3, lLongitude_3,
                                       lLatitude_4, lLongitude_4))
    {
      SerialUSB.println(F("$ACK,CFGZN*33"));
    }
    else
    {
      SerialUSB.println(F(ERROR_MESSAGE));
    }
  }
  else if (m_sBuffer.substring(0, m_sBuffer.indexOf(',')) == "$RDZN")  // Lecture d'une zone d'activation
  {
    SerialUSB.println(F("$ACK,RDZN*67"));
  }
}

