#include "Controleur.h"
#include <Arduino.h>

// Affichage débug !!!!!!!! A DESACTIVER ABSOLUEMENT EN STANDALONE !!!!!!!!! (pas de connexion Série = blocage sur le 'while (!SerialUSB);')
//#define DEBUG_VERBOSE

Controleur::Controleur()
{
  m_pConfig = new Config(this);
  m_pCanon = new Canon();
  m_pGPS = new GPS();
  m_pMemoire = new Memoire();
}

void Controleur::init()
{ 
  m_pConfig->init();
  if (!m_pConfig->isConfiguring())  // Mode nominal
  {
    m_pMemoire->readZone(0);
    m_pMemoire->readZone(1);
    m_pCanon->init();
    m_pGPS->init();
    // Load zones
  }
}

void Controleur::loop()
{
  if (m_pConfig->isConfiguring()) // Mode configuration
  {
    m_pConfig->read();
  }
  else  // Mode nominal
  {
    m_pGPS->read();
  }
}

bool Controleur::clrZonesInMemory()
{
  m_pMemoire->clrZones();
  return true;
}

bool Controleur::addZoneInMemory(long lLatitude_1, long lLongitude_1,
                                 long lLatitude_2, long lLongitude_2,
                                 long lLatitude_3, long lLongitude_3,
                                 long lLatitude_4, long lLongitude_4)
{
  return m_pMemoire->writeZone(lLatitude_1, lLongitude_1,
                               lLatitude_2, lLongitude_2,
                               lLatitude_3, lLongitude_3,
                               lLatitude_4, lLongitude_4);
}
    
