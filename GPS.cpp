#include "GPS.h"

// Affichage débug !!!!!!!! A DESACTIVER ABSOLUEMENT EN STANDALONE !!!!!!!!! (pas de connexion Série = blocage sur le 'while (!SerialUSB);')
//#define DEBUG_VERBOSE

#define BAUD_RATE 57600

#define PMTK_SET_NMEA_UPDATE_100_MILLIHERTZ  "$PMTK220,10000*2F" // Once every 10 seconds, 100 millihertz.
#define PMTK_API_SET_FIX_CTL_100_MILLIHERTZ  "$PMTK300,10000,0,0,0,0*2C" // Once every 10 seconds, 100 millihertz.
#define PMTK_SET_NMEA_OUTPUT_RMCONLY "$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29"
#define PMTK_SET_BAUD_57600 "$PMTK251,57600*2C"

#define ENABLE_GPS  2

GPS::GPS()
  :m_ReceivedOneMsg(false)
  ,m_Enable(false)
  ,m_Status('V')
  ,m_lLatitude(0)
  ,m_lLongitude(0)
{
}

void GPS::init()
{
#ifdef DEBUG_VERBOSE
  SerialUSB.print("Init GPS...");
#endif
  Serial5.begin(9600);
//  Serial5.println(F(PMTK_SET_NMEA_UPDATE_100_MILLIHERTZ));
//  Serial5.println(F(PMTK_API_SET_FIX_CTL_100_MILLIHERTZ));
  Serial5.println(F(PMTK_SET_NMEA_OUTPUT_RMCONLY));

  m_sBuffer = "";

  pinMode(ENABLE_GPS, OUTPUT);
  enable(true);

#ifdef DEBUG_VERBOSE
  SerialUSB.println(" OK");
#endif
}


void GPS::read()
{
  if (Serial5.available())
  {
    char c = Serial5.read();

#ifdef DEBUG_VERBOSE
    SerialUSB.print(c);
#endif
    if (c == '\n')
    {
//      m_sBuffer = "$GPRMC,091523.757,A,4337.1121,N,00126.2174,E,0.22,325.69,060612,,,A*6A";
//      m_sBuffer = "$GPGGA,064036.289,4836.5375,N,00740.9373,E,1,04,3.2,200.2,M,,,,,,0000*0E";
#ifdef DEBUG_VERBOSE
      SerialUSB.println(m_sBuffer);
#endif
      if (messageIsValid())
      {
        parseNMEA();
      }
      m_sBuffer = "";
    }
    else
    {
      m_sBuffer += c;
    }
  }
}

bool GPS::isValid()
{
  return (m_ReceivedOneMsg && m_Status == 'A');
}

long GPS::getLatitude()
{
  return m_lLatitude;
}

long GPS::getLongitude()
{
  return m_lLongitude;
}

void GPS::enable(bool enable)
{
  if (enable != m_Enable)
  {
    m_Enable = enable;
    if (enable)
    {
      m_ReceivedOneMsg = false;
      digitalWrite(ENABLE_GPS, HIGH);
#ifdef DEBUG_VERBOSE
      SerialUSB.println("Activation du GPS");
#endif
    }
    else
    {
      digitalWrite(ENABLE_GPS, LOW);
#ifdef DEBUG_VERBOSE
      SerialUSB.println("Desactivation du GPS");
#endif
    }
  }
}

bool GPS::messageIsValid()
{
  char checksum = 0;
  for (int i = 1 ; i < m_sBuffer.indexOf('*') ; i++)
  {
    checksum ^= m_sBuffer.charAt(i);
  }
  String str_checksum = String(checksum, HEX);
  if (str_checksum.length() == 1)
  {
    str_checksum = '0' + str_checksum;
  }

  int indexOfChecksum = m_sBuffer.indexOf('*') + 1;
#ifdef DEBUG_VERBOSE
  SerialUSB.print("Calculated Checksum = ");
  SerialUSB.println(str_checksum);
  SerialUSB.print("Received Checksum = ");
  SerialUSB.println(m_sBuffer.substring(indexOfChecksum, indexOfChecksum + 2));
#endif

  if (m_sBuffer.substring(indexOfChecksum, indexOfChecksum + 2).equalsIgnoreCase(str_checksum))
    return true;

#ifdef DEBUG_VERBOSE
  SerialUSB.println("Message is not valid");
#endif
  return false;
}

void GPS::parseNMEA()
{
#ifdef DEBUG_VERBOSE
  SerialUSB.print("Message type = ");
  SerialUSB.println(m_sBuffer.substring(0, m_sBuffer.indexOf(',')));
#endif
  if (m_sBuffer.substring(0, m_sBuffer.indexOf(',')) == "$GPRMC")
  {
    m_sBuffer.remove(0, m_sBuffer.indexOf(',') + 1); // Remove Message Type
    m_sBuffer.remove(0, m_sBuffer.indexOf(',') + 1); // Remove UTC

    m_Status = m_sBuffer[0];
    m_sBuffer.remove(0, m_sBuffer.indexOf(',') + 1); // Remove GPS State

#ifdef DEBUG_VERBOSE
    SerialUSB.print("GPS State = ");
    SerialUSB.println(m_Status);
#endif

    m_lLatitude = m_sBuffer.substring(0, 2).toInt() * 1000000L;
    m_sBuffer.remove(0, 2);  // Remove Latitude degrees

    m_lLatitude += ((m_sBuffer.substring(0, 2) + m_sBuffer.substring(3, 7)).toInt() * 5L) / 3L;
    m_sBuffer.remove(0, m_sBuffer.indexOf(',') + 1);  // Remove Latitude minutes

    m_lLatitude *= m_sBuffer[0] == 'N' ? 1 : -1;
    m_sBuffer.remove(0, m_sBuffer.indexOf(',') + 1);  // Remove Latitude orientation

#ifdef DEBUG_VERBOSE
    SerialUSB.print("Latitude = ");
    SerialUSB.print(m_lLatitude / 1000000L);
    SerialUSB.print(".");
    SerialUSB.println(abs(m_lLatitude) % 1000000L);
#endif

    m_lLongitude = m_sBuffer.substring(0, 3).toInt() * 1000000L;
    m_sBuffer.remove(0, 3);  // Remove Latitude degrees

    m_lLongitude += ((m_sBuffer.substring(0, 2) + m_sBuffer.substring(3, 7)).toInt() * 5L) / 3L;
    m_sBuffer.remove(0, m_sBuffer.indexOf(',') + 1);  // Remove Latitude minutes

    m_lLongitude *= m_sBuffer[0] == 'E' ? 1 : -1;
    //m_sBuffer.remove(0, m_sBuffer.indexOf(',') + 1);  // Remove Longitude orientation

#ifdef DEBUG_VERBOSE
    SerialUSB.print("Longitude = ");
    SerialUSB.print(m_lLongitude / 1000000L);
    SerialUSB.print(".");
    SerialUSB.println(abs(m_lLongitude) % 1000000L);
#endif

    m_ReceivedOneMsg = true;
  }
}



