#include "Config.h"
#include "Canon.h"
#include "GPS.h"
#include "Position.h"

// Affichage débug !!!!!!!! A DESACTIVER ABSOLUEMENT EN STANDALONE !!!!!!!!! (pas de connexion Série = blocage sur le 'while (!SerialUSB);')
#define DEBUG_VERBOSE

Controleur myControleur;

void setup() {
  
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  
#ifdef DEBUG_VERBOSE
  SerialUSB.begin(9600);
  while (!SerialUSB);
  SerialUSB.println("Gedo V1.0 - DEBUG_VERBOSE\n");
#endif

  myControleur.init();
  
}

void loop()
{
  myControleur.loop();
}
