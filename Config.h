#ifndef _CONFIG_H
#define _CONFIG_H
#include <Arduino.h>

#include "Controleur.h"

class Controleur;

class Config {
  private:
    Controleur*  m_pControleur;
    bool         m_IsConfiguring;
    String       m_sBuffer;
  
  public:
    Config(Controleur *pControleur);
    void init();
    void read();  // fonction à appeler dans la boucle principale
    bool isConfiguring();
    
  private:
    bool messageIsValid();
    void parseCommand();
};
#endif

