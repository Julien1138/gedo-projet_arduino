#ifndef _MEMOIRE_H
#define _MEMOIRE_H

#include "FlashStorage.h"

class Memoire {
  private:
    //FlashStorageClass m_NbrOfZones<int>;

  public:
    Memoire();
    
    void clrZones();
    bool writeZone(long lLatitude_1, long lLongitude_1,
                   long lLatitude_2, long lLongitude_2,
                   long lLatitude_3, long lLongitude_3,
                   long lLatitude_4, long lLongitude_4);
    void readZone(int zoneNbr);
};
#endif

