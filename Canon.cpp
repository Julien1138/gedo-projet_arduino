#include "Canon.h"
#include <Arduino.h>

// Affichage débug !!!!!!!! A DESACTIVER ABSOLUEMENT EN STANDALONE !!!!!!!!! (pas de connexion Série = blocage sur le 'while (!SerialUSB);')
//#define DEBUG_VERBOSE

#define SET_RELAIS    9
#define RESET_RELAIS  6
#define CMD_TIME      100 // temps de commande du relai bistable (ms)

Canon::Canon()
{
}

void Canon::init()
{
  setCanon(1);
}

void Canon::setCanon(unsigned char state)  // 0 = Canon éteint, 1 = Canon allumé
{
  if (state = 1)
  {
    digitalWrite(SET_RELAIS, HIGH);
#ifdef DEBUG_VERBOSE
    SerialUSB.println("Activation du canon");
#endif
    delay(CMD_TIME);                    // waits for 100 ms
    digitalWrite(SET_RELAIS, LOW);
  }
  else
  {
    digitalWrite(RESET_RELAIS, HIGH);
#ifdef DEBUG_VERBOSE
    SerialUSB.println("Desactivation du canon");
#endif
    delay(CMD_TIME);                    // waits for 100 ms
    digitalWrite(RESET_RELAIS, LOW);
  }
}

