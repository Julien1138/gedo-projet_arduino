#include "Memoire.h"
#include <Arduino.h>

// Affichage débug !!!!!!!! A DESACTIVER ABSOLUEMENT EN STANDALONE !!!!!!!!! (pas de connexion Série = blocage sur le 'while (!SerialUSB);')
#define DEBUG_VERBOSE

typedef struct {
  long lLatitude_1;
  long lLongitude_1;
  long lLatitude_2;
  long lLongitude_2;
  long lLatitude_3;
  long lLongitude_3;
  long lLatitude_4;
  long lLongitude_4;
} ZoneInMemory;

FlashStorage(m_NbrOfZones, int);
FlashStorage(m_Zone00, ZoneInMemory);
FlashStorage(m_Zone01, ZoneInMemory);
FlashStorage(m_Zone02, ZoneInMemory);
FlashStorage(m_Zone03, ZoneInMemory);
FlashStorage(m_Zone04, ZoneInMemory);
FlashStorage(m_Zone05, ZoneInMemory);
FlashStorage(m_Zone06, ZoneInMemory);
FlashStorage(m_Zone07, ZoneInMemory);
FlashStorage(m_Zone08, ZoneInMemory);
FlashStorage(m_Zone09, ZoneInMemory);
FlashStorage(m_Zone10, ZoneInMemory);
FlashStorage(m_Zone11, ZoneInMemory);
FlashStorage(m_Zone12, ZoneInMemory);
FlashStorage(m_Zone13, ZoneInMemory);
FlashStorage(m_Zone14, ZoneInMemory);
FlashStorage(m_Zone15, ZoneInMemory);

Memoire::Memoire()
{
}

void Memoire::clrZones()
{
#ifdef DEBUG_VERBOSE
  SerialUSB.println("Clearing zones in memory.");
#endif
  m_NbrOfZones.write(0);
}

bool Memoire::writeZone(long lLatitude_1, long lLongitude_1,
                        long lLatitude_2, long lLongitude_2,
                        long lLatitude_3, long lLongitude_3,
                        long lLatitude_4, long lLongitude_4)
{
#ifdef DEBUG_VERBOSE
  SerialUSB.println("Writing a new zone in memory.");
#endif

  int nbrOfZones = m_NbrOfZones.read();
  if (nbrOfZones > 15)
  {
    return false;
  }
  
  ZoneInMemory zone;
  zone.lLatitude_1 = lLatitude_1;
  zone.lLongitude_1 = lLongitude_1;
  zone.lLatitude_2 = lLatitude_2;
  zone.lLongitude_2 = lLongitude_2;
  zone.lLatitude_3 = lLatitude_3;
  zone.lLongitude_3 = lLongitude_3;
  zone.lLatitude_4 = lLatitude_4;
  zone.lLongitude_4 = lLongitude_4;
#ifdef DEBUG_VERBOSE
  SerialUSB.print(" Writing zone #");
  SerialUSB.println(nbrOfZones);
#ifdef DEBUG_VERBOSE
  SerialUSB.print("  lLatitude_1 = ");
  SerialUSB.println(zone.lLatitude_1);
  SerialUSB.print("  lLongitude_1 = ");
  SerialUSB.println(zone.lLongitude_1);
  SerialUSB.print("  lLatitude_2 = ");
  SerialUSB.println(zone.lLatitude_2);
  SerialUSB.print("  lLongitude_2 = ");
  SerialUSB.println(zone.lLongitude_2);
  SerialUSB.print("  lLatitude_3 = ");
  SerialUSB.println(zone.lLatitude_3);
  SerialUSB.print("  lLongitude_3 = ");
  SerialUSB.println(zone.lLongitude_3);
  SerialUSB.print("  lLatitude_4 = ");
  SerialUSB.println(zone.lLatitude_4);
  SerialUSB.print("  lLongitude_4 = ");
  SerialUSB.println(zone.lLongitude_4);
#endif
#endif
  switch(nbrOfZones)
  {
    case 0 :
      m_Zone00.write(zone);
      break;
    case 1 :
      m_Zone01.write(zone);
      break;
    case 2 :
      m_Zone02.write(zone);
      break;
    case 3 :
      m_Zone03.write(zone);
      break;
    case 4 :
      m_Zone04.write(zone);
      break;
    case 5 :
      m_Zone05.write(zone);
      break;
    case 6 :
      m_Zone06.write(zone);
      break;
    case 7 :
      m_Zone07.write(zone);
      break;
    case 8 :
      m_Zone08.write(zone);
      break;
    case 9 :
      m_Zone09.write(zone);
      break;
    case 10 :
      m_Zone10.write(zone);
      break;
    case 11 :
      m_Zone11.write(zone);
      break;
    case 12 :
      m_Zone12.write(zone);
      break;
    case 13 :
      m_Zone13.write(zone);
      break;
    case 14 :
      m_Zone14.write(zone);
      break;
    case 15 :
      m_Zone15.write(zone);
      break;
    default :
      return false;
      break;
  }
  nbrOfZones++;
  m_NbrOfZones.write(nbrOfZones);
  return true;
}

void Memoire::readZone(int zoneNbr)
{
#ifdef DEBUG_VERBOSE
  SerialUSB.print("Reading zone #");
  SerialUSB.println(zoneNbr);
#endif

  ZoneInMemory zone;
  switch(zoneNbr)
  {
    case 0 :
      zone = m_Zone00.read();
      break;
    case 1 :
      zone = m_Zone01.read();
      break;
    case 2 :
      zone = m_Zone02.read();
      break;
    case 3 :
      zone = m_Zone03.read();
      break;
    case 4 :
      zone = m_Zone04.read();
      break;
    case 5 :
      zone = m_Zone05.read();
      break;
    case 6 :
      zone = m_Zone06.read();
      break;
    case 7 :
      zone = m_Zone07.read();
      break;
    case 8 :
      zone = m_Zone08.read();
      break;
    case 9 :
      zone = m_Zone09.read();
      break;
    case 10 :
      zone = m_Zone10.read();
      break;
    case 11 :
      zone = m_Zone11.read();
      break;
    case 12 :
      zone = m_Zone12.read();
      break;
    case 13 :
      zone = m_Zone13.read();
      break;
    case 14 :
      zone = m_Zone14.read();
      break;
    case 15 :
      zone = m_Zone15.read();
      break;
    default :
      break;
  }
  
#ifdef DEBUG_VERBOSE
  SerialUSB.print("  lLatitude_1 = ");
  SerialUSB.println(zone.lLatitude_1);
  SerialUSB.print("  lLongitude_1 = ");
  SerialUSB.println(zone.lLongitude_1);
  SerialUSB.print("  lLatitude_2 = ");
  SerialUSB.println(zone.lLatitude_2);
  SerialUSB.print("  lLongitude_2 = ");
  SerialUSB.println(zone.lLongitude_2);
  SerialUSB.print("  lLatitude_3 = ");
  SerialUSB.println(zone.lLatitude_3);
  SerialUSB.print("  lLongitude_3 = ");
  SerialUSB.println(zone.lLongitude_3);
  SerialUSB.print("  lLatitude_4 = ");
  SerialUSB.println(zone.lLatitude_4);
  SerialUSB.print("  lLongitude_4 = ");
  SerialUSB.println(zone.lLongitude_4);
#endif
}
