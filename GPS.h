#ifndef _GPS_H
#define _GPS_H
#include <Arduino.h>

class GPS {
  private:
    String m_sBuffer;
    bool m_ReceivedOneMsg;
    bool m_Enable;
    char m_Status;  // 'A' = active, 'V' = void
    long m_lLatitude;  // 1 000 000 th of degree
    long m_lLongitude;  // 1 000 000 th of degree
  
  public:
    GPS();
    void init();
    void read();  // fonction à appeler dans la boucle principale
    bool isValid();
    long getLatitude();
    long getLongitude();
    void enable(bool enable);
    
  private:
    bool messageIsValid();
    void parseNMEA();
};
#endif

